# ROCKCOTE structure common

Common ROCKCOTE data structures and functionality.

Depends on

* [ROCKCOTE users](https://bitbucket.org/rockcote/rockcote_user)

Modules that depend on that module

* ROCKCOTE salesforce
* ROCKCOTE structure common 

## Installation

Add the following lines to `repositories` section of `composer.json`

```
  ...
  "repositories": [
    {
      "type": "composer",
      "url": "https://packages.drupal.org/8"
    },
    {
      "type": "vcs",
      "url": "https://bitbucket.org/rockcote/rockcote_user.git"
    },
    {
      "type": "vcs",
      "url": "https://bitbucket.org/rockcote/rockcote_structure_common.git"
    }
  ],
  ...
  "extra": {
    "installer-paths": {
      ...
      "web/modules/custom/{$name}": [
        "type:drupal-module-custom"
      ],
      ...
    }
  }
  ...
```

Run composer command

```
  composer require rockcote/rockcote_structure_common
```

Additionally, you can exclude current module in `.gitignore`

```
/web/modules/custom/rockcote_structure_common
/web/modules/custom/rockcote_user
```

## Updating

To update to the latest version run

```
composer update rockcote/rockcote_structure_common --with-dependencies
```

## Issues

Move to [bitbucket](https://bitbucket.org/rockcote/rockcote_structure_common) from [gitlab](https://gitlab.com/rockcote-aus/rockcote_structure_common) due to the [composer issue](https://github.com/composer/composer/issues/6642) and [gitlab issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/37355).

## Repositories

* [Bitbucket](https://bitbucket.org/rockcote/rockcote_structure_common): active
* [GitLab](https://gitlab.com/rockcote-aus/rockcote_structure_common): composer prevents from using GitLab as of 04 Sep 2017. See [issues](#issues).
