<?php

namespace Drupal\rockcote_structure_common\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\node\NodeInterface;
use Dompdf\Dompdf;

/**
 * Returns responses for Specification routes.
 */
class RockcoteNodeController extends ControllerBase {

  /**
   * Route matching service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Renderer matching service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  private $renderer;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $etm;

  /**
   * Constructs a new RockcoteSpecificationController.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   Route matching service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Route matching service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(RouteMatchInterface $route_match, RendererInterface $renderer, EntityTypeManagerInterface $entity_type_manager) {
    $this->routeMatch = $route_match;
    $this->renderer = $renderer;
    $this->etm = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('renderer'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Generates PDF specification.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Node entity.
   */
  public function printPdf(NodeInterface $node) {
    $dompdf = new Dompdf();

    // Render page and print to PDF.
    $page = node_view($node, 'default');
    $html = $this->renderer->render($page);

    $html = str_replace('<article', '<div', $html);
    $html = str_replace('article>', 'div>', $html);

    $dompdf->loadHtml($html);

    // (Optional) Setup the paper size and orientation.
    $dompdf->setPaper('A4', 'portrait');

    // Render the HTML as PDF.
    $dompdf->render();

    // Output the generated PDF to Browser.
    // $dompdf->stream();.
    $response = new Response();

    $response->setContent($dompdf->stream($node->getTitle() . '.pdf', [
      'Attachment' => FALSE,
    ]));
    $response->setStatusCode(200);
    $response->headers->set('Content-Type', 'application/pdf');

    return $response;
  }

}
