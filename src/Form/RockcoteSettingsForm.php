<?php

namespace Drupal\rockcote_structure_common\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure settings for WMQ Workflow.
 */
class RockcoteSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rockcote_rockcote_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'rockcote_settings.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('rockcote_settings.settings');

    $form['rockcote_specifier_sds_1_text'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('SDS text after section 1'),
      '#default_value' => $config->get('rockcote_specifier_sds_1_text')['value'],
    ];

    $form['rockcote_specifier_sds_16_text'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('SDS text after section 16'),
      '#default_value' => $config->get('rockcote_specifier_sds_16_text')['value'],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save new settings.
    $this->config(reset($this->getEditableConfigNames()))
      ->set('rockcote_specifier_sds_1_text', $form_state->getValue('rockcote_specifier_sds_1_text'))
      ->set('rockcote_specifier_sds_16_text', $form_state->getValue('rockcote_specifier_sds_16_text'))
      ->save();
    parent::submitForm($form, $form_state);

  }

}
