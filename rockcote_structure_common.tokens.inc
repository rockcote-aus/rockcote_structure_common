<?php

/**
 * @file
 * Builds placeholder replacement tokens for node-related data.
 */

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\node\Entity\Node;

/**
 * Implements hook_token_info_alter().
 */
function rockcote_structure_common_token_info_alter(&$data) {
  // Rockcote tokens.
  $data['tokens']['site']['page-uuid'] = [
    'name' => t('UUID of the current page'),
    'description' => t('Checks if the current page has UUID.'),
  ];
  $data['tokens']['site']['page-nid'] = [
    'name' => t('Node id of the current page'),
    'description' => t('Checks if the current page has node ID.'),
  ];
  $data['tokens']['site']['page-destination'] = [
    'name' => t('Current page destination'),
    'description' => t('Content of the q parameter e.g. everything after URL.'),
  ];
  $data['tokens']['node']['uuid'] = [
    'name' => t("Node UUID"),
    'description' => t('The unique ID of the content node.'),
  ];
}

/**
 * Implements hook_tokens().
 */
function rockcote_structure_common_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  foreach ($tokens as $name => $original) {

    if ($type == 'site') {
      switch ($name) {
        case 'page-uuid':
        case 'page-nid':
          // Get UUID of the current node.
          $path = ltrim(\Drupal::service('path.current')->getPath(), '/');
          $path = \Drupal::service('path.alias_manager')->getPathByAlias($path);
          $id = '';
          if ((strpos($path, 'node/') === 0) || (strpos($path, 'specification/') === 0)) {
            $path_split = explode('/', $path);
            $node = Node::load($path_split[1]);
            if ($node) {
              $id = ($name === 'page-uuid') ? $node->uuid() : $node->id();
            }
          }
          $replacements[$original] = $id;
          break;

        case 'page-destination':
          $replacements[$original] = \Drupal::service('path.current')->getPath();
          break;

      }
    }
    if ($type == 'node' && !empty($data['node'])) {
      /** @var \Drupal\node\NodeInterface $node */
      $node = $data['node'];

      if ($name == 'uuid') {
        $replacements[$original] = $node->uuid();
      }
    }
  }

  return $replacements;
}
